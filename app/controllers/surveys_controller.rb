require 'csv'

class SurveysController < ApplicationController
  
  before_action :logged_in_user, only: [:update, :destroy, :create]
  before_action :correct_or_admin_user, only: :destroy
  
  def destroy
    Survey.find(params[:id]).destroy
    flash[:success] = "データを削除しました。"
    redirect_to surveys_url
  end
  
  def index
    @surveys = Survey.all
  end
    
  def show
    @survey = Survey.find(params[:id])
  end
    
  def new
    @survey = Survey.new
  end
    
  def create
    @survey = current_user.surveys.new(survey_params)

    if (!@survey.valid?)
      flash.now[:danger] = "調査名を設定してください。調査名は50文字以下にしてください。"
      render 'new' and return
    end
    
    #if (Survey.find_by(name: @survey.name) != nil)
    #  flash.now[:danger] = "調査名は重複できません。"
    #  render 'new' and return
    #end
    
    if (@survey.file == nil)
      flash.now[:danger] = "ファイルを選択してください。"
      render 'new' and return
    end
    
    begin
    ActiveRecord::Base.transaction do
      @survey.save!

      CSV.foreach(@survey.file.path, { encoding: "cp932:utf-8", headers: true, skip_blanks: true }) do |row|
        data = @survey.data.new(
          site_name: row["調査地点名称"],
          researcher_name: row["調査者名"],
          date: row["日付"],
          latitude: row["緯度"].to_f,
          longitude: row["経度"].to_f,
          value1: row["流れる水の量"].to_f,
          value2: row["岸のようす"].to_f,
          value3: row["魚が川を遡れるか"].to_f,
          value4: row["川原と水辺の植物"].to_f,
          value5: row["鳥の生息、すみ場"].to_f,
          value6: row["魚の生息、すみ場"].to_f,
          value7: row["川底の様子と底生生物"].to_f,
          value8: row["透視度"].to_f,
          value9: row["水のにおい"].to_f,
          value10: row["COD"].to_f,
          value11: row["けしき(感じる)"].to_f,
          value12: row["ごみ(見る)"].to_f,
          value13: row["水との触れ合い"].to_f,
          value14: row["川のかおり(かぐ)"].to_f,
          value15: row["川の音(聞く)"].to_f,
          value16: row["歴史と文化"].to_f,
          value17: row["水辺の近づきやすさ"].to_f,
          value18: row["日常的な利用"].to_f,
          value19: row["産業などの活動"].to_f,
          value20: row["環境の活動"].to_f,
          value21: row["自然なすがた"].to_f,
          value22: row["豊かな生き物"].to_f,
          value23: row["水のきれいさ"].to_f,
          value24: row["快適な水辺"].to_f,
          value25: row["地域とのつながり"].to_f
        )
        data.save!
      end
    end
      flash[:success] = "データを登録しました。"
      redirect_to @survey
    rescue => e
      flash.now[:danger] = e.message
      render 'new'
    end
  end
  
  
    private

    def survey_params
      params.require(:survey).permit(:name, :file)
    end
    
    def correct_or_admin_user
      unless current_user.admin?
        @survey = current_user.surveys.find_by(id: params[:id])
        redirect_to root_url if @survey.nil?
      end
    end
    
end
