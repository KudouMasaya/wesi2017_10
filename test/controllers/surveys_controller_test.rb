require 'test_helper'

class SurveysControllerTest < ActionDispatch::IntegrationTest
  def setup
    @survey = surveys(:one)
  end

  test "should redirect create when not logged in" do
    assert_no_difference 'Survey.count' do
      post surveys_path, params: { survey: { name: "dummy" } }
    end
    assert_redirected_to login_url
  end

  test "should redirect destroy when not logged in" do
    assert_no_difference 'Survey.count' do
      delete survey_path(@survey)
    end
    assert_redirected_to login_url
  end
    
  test "should redirect destroy for wrong survey" do
    log_in_as(users(:archer))
    survey = surveys(:one)
    assert_no_difference 'Survey.count' do
      delete survey_path(survey)
    end
    assert_redirected_to root_url
  end
  
  test "アドミンは他の人の調査データを削除できる" do
    log_in_as(users(:michael))
    survey = surveys(:two)
    assert_difference 'Survey.count', -1 do
      delete survey_path(survey)
    end
  end
  
end
