require 'test_helper'

class ApplicationHelperTest < ActionView::TestCase
  test "full title helper" do
    assert_equal full_title,         "水しるべ調査（試験運用中）"
    assert_equal full_title("Help"), "Help | 水しるべ調査（試験運用中）"
  end
end