require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
  end

  test "ログインしていないユーザのリンク" do
    get root_path
    assert_template 'static_pages/home'
    assert_select "a[href=?]", root_path, count: 2
    assert_select "a[href=?]", surveys_path
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", help_path
    get surveys_path
    assert_select "title", full_title("調査一覧")
    get login_path
    assert_select "title", full_title("ログイン")
  end

  test "ログインしているユーザのリンク" do
    log_in_as(@user)
    get user_path(@user)
    assert_template 'users/show'
    assert_select "a[href=?]", root_path, count: 2
    assert_select "a[href=?]", surveys_path
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", help_path
    assert_select "a[href=?]", users_path
    assert_select "a[href=?]", user_path(@user)
    assert_select "a[href=?]", edit_user_path(@user)
  end
end