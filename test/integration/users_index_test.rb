require 'test_helper'

class UsersIndexTest < ActionDispatch::IntegrationTest
  def setup
    @admin     = users(:michael)
    @non_admin = users(:archer)
  end

  test "ログインしていない場合は表示不可" do
    get user_path(@non_admin)
    assert_redirected_to login_url
  end

  test "一般ユーザは表示不可" do
    log_in_as(@non_admin)
    get users_path
    assert_redirected_to root_path
  end

  test "管理者は表示可、削除リンクあり" do
    log_in_as(@admin)
    get users_path
    assert_template 'users/index'
    users.each do |user|
      assert_select 'a[href=?]', user_path(user), text: user.name
      unless user == @admin
        assert_select 'a[href=?]', user_path(user), text: '管理者がアカウントを削除'
      end
    end
    assert_difference 'User.count', -1 do
      delete user_path(@non_admin)
    end
  end
end