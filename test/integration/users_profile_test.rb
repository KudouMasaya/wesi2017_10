require 'test_helper'

class UsersProfileTest < ActionDispatch::IntegrationTest
  include ApplicationHelper

  def setup
    @admin     = users(:michael)
    @non_admin = users(:archer)
  end

  test "ログインしていない場合は表示不可" do
    get user_path(@non_admin)
    assert_redirected_to login_url
  end

  test "他人のプロフィールは表示不可" do
    log_in_as(@non_admin)
    get user_path(@admin)
    assert_redirected_to root_path
  end

  test "自分のプロフィールは表示可能" do
    log_in_as(@non_admin)
    get user_path(@non_admin)
    assert_template 'users/show'
    assert_select 'title', full_title('ユーザー情報')
    assert_select 'p', text: 'アカウントの名前：' + @non_admin.name
    assert_select 'p', text: 'メールアドレス：' + @non_admin.email
  end

  test "adminは他人も表示可能" do
    log_in_as(@admin)
    get user_path(@non_admin)
    assert_template 'users/show'
    assert_select 'title', full_title('ユーザー情報')
    assert_select 'p', text: 'アカウントの名前：' + @non_admin.name
    assert_select 'p', text: 'メールアドレス：' + @non_admin.email
  end
end
