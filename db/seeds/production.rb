User.create!(name:  "管理者",
        email: "admin@example.test",
        password:              "admin88user",
        password_confirmation: "admin88user",
        admin: true)

User.create!(name:  "角田均(管理者)",
        email: "tsunoda@aomori-u.ac.jp",
        password:              "admin88user",
        password_confirmation: "admin88user",
        admin: true)
        
User.create!(name:  "工藤誠也(管理者)",
        email: "sohuto2kudou@yahoo.co.jp",
        password:              "admin88user",
        password_confirmation: "admin88user",
        admin: true)